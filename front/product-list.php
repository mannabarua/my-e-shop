<?php  

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/front/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
  
?>

<?php 

	use Eshop\Product\Product;
    $product = new Product();
    $products = $product->all();

 ?>


<?php ob_start(); ?>
 <!-- Feature Category section Start -->		
 <section>
 	<div class="container">
 		<div class="row">
			<div class="col-12">
				<div class="top_page_link">
					<h2><a href="">Home</a> > <a href="">categroy one</a></h2>
				</div>        
			</div>      
 		</div>
 	</div>
 </section>
 <!-- Feature Category section End -->
  
 <!-- Feature Product section Start -->		
 <section>
	<div class="container">
		<div class="product-list-section-portion">
			<div class="row">
				<div class="col-12">
					<div class="section-heading">
						<h2>FEATURE Product</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<?php foreach ($products as $product): ?>
					<div class="col-md-3">				 	
						<div class="product-item mb-5">
							<div class="card">
								<div class="card-body">
									<div class="product">
										<form action="cart.php" method="POST">
											<input type="hidden" value="<?= $product->product_id; ?>" name="product_id">
											<input type="hidden" value="<?= $product->product_id; ?>" name="product_id">
											<img src="<?=IMG?><?= $product->picture; ?>" alt="image" class="img-fluid">
											<h2><?= $product->title; ?></h2>
											<div class="price-box">
												<del><?= $product->mrp; ?></del> <span><?= $product->total_sales; ?></span>
											</div>
											<div class="product-action-box">
												<button class="btn btn-danger btn-sm" type="submit">Add to bag</button>
												<a href="<?= PRODUCTDETAIL.'product-details.php?id='.$product->product_id;?>">View Details</a>
											</div>
										</form>
									</div>
								</div>								
							</div>	
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
 </section>
 <!-- Feature Prduct section End -->

<?php

    $product_list = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $product_list, $layout)


 ?>