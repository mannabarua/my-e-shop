  <footer id="footer_section">
    <div class="container">
      <div class="row">
          <!-- Footer Logo and text section Start -->
        <div class="col-md-4">
          <div class="footer_logo_portion">
            <img src="<?= IMG?>/logo.png" alt="logo">
            <ul>
              <li>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-twitter"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-pinterest-p"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-youtube"></i></a>
              </li>
            </ul>            
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          </div>
        </div>
          <!-- Footer Logo and text section End -->
          <!-- Footer About Company Menu Start -->
        <div class="col-md-2 offset-md-3">
          <div class="footer_about_portion">
            <h2>About Jishop</h2>
            <ul>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">News</a>
              </li>
              <li>
                <a href="#">About Security</a>
              </li>
              <li>
                <a href="#">Fees Details</a>
              </li>
              <li>
                <a href="#">Terms & Condition</a>
              </li>              
              <li>
                <a href="#">Privacy Policy</a>
              </li>             
            </ul>
          </div>
        </div>
          <!-- Footer About Company Menu End -->
          <!-- Footer Support Menu Start -->
        <div class="col-md-2 offset-md-1">
          <div class="footer_about_portion">
            <h2>Support</h2>
            <ul>
              <li>
                <a href="#">Contact Us</a>
              </li>
              <li>
                <a href="#">Customer Care</a>
              </li>
              <li>
                <a href="#">FAQ</a>
              </li>
              <li>
                <a href="#">Resource Center</a>
              </li>
              <li>
                <a href="#">Forums</a>
              </li>              
              <li>
                <a href="#">Community</a>
              </li>
             
            </ul>
          </div>
        </div>
          <!-- Footer Support Menu End -->
      </div>
    </div>
  </footer>
  <!-- Footer section End -->
  
  
  <script src="<?= JS ?>modernizr-3.6.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="<?= JS ?>jquery-3.3.1.min.js"><\/script>')</script>
  <script src="<?= JS ?>plugins.js"></script>
  <script src="<?= JS ?>bootstrap.min.js"></script>
  <script src="<?= JS ?>stellarnav.js"></script>
  <script src="<?= JS ?>owl.carousel.min.js"></script>
  <script src="<?= JS ?>front_main.js"></script>

  <script>
	


$('#owl-one').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:4
        }
    }
})




var owl = $('.owl-carousel');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[4000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})

  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>

</html>