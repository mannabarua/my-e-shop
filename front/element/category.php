<?php $categories = $category->all(); ?>



 <section>
 	<div class="container">
 		<div class="section-portion">
 			<div class="row">
 				<div class="col-12">
 					<div class="section-heading">
 						<h2>FEATURE CATEGORY</h2>
 					</div>
 				</div>
 			</div>
 			<div class="row">
 				<?php foreach ($categories as $category): ?>

 				<div class="col-md-4">
 					<div class="category-name">
 						<a href="#">
 							<div class="field-hover">
	 							<img src="<?= IMG ?>category.jpg" alt="image">
	 							<h2><?= $category->name; ?></h2>
 							</div>
 							
 						</a> 						
 					</div>
 				</div>
 				
 				<?php endforeach; ?>
 			</div>
 		</div>
 	</div>
 </section>