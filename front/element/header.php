<?php

	use Eshop\Cart\Cart;

	$cart = new Cart();

?>
<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= CSS?>fontawesome/css/all.min.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="<?= CSS ?>owl.carousel.min.css">
  <link rel="stylesheet" href="<?= CSS ?>owl.theme.default.min.css">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= CSS ?>bootstrap.min.css">
  <link rel="stylesheet" href="<?= CSS ?>stellarnav.css">  

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="<?= CSS ?>front_main.css">
  
</head>

<body>
	<!-- Header Start -->
  <header> 
	<!-- Top Bar Start -->
	<div class="top_bar">
		<div class="container">
			<div class="row align-items-center">
				<!-- Logo -->
				<div class="col-md-4 ">
					<div class="logo mx-auto mx-md-0">
						<a href="<?= WEBROOT ?>front/index.php"><img src="<?= IMG ?>logo.png" alt="logo"  /></a>
					</div>
				</div>
				<!-- Advertise Image top bar -->
				<div class="col-md-8"> 
					<div class="top_search ">
						<div class="input-group">
						  <input type="search" class="form-control" placeholder="Search">
						  <div class="input-group-append">
						    <button class="btn btn-topbar" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button> 
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	  </header>
	<!-- Top Bar End -->
	<!-- Menu Bar Start -->
	<div class="menu_bar sticky-top">
		<div class="container">
			<div class="row align-items-center">
				<!-- Menu Item Start -->
				<div class="col-md-4 col-2">
					<nav class="navbar navbar-expand-md navbar-dark stellarnav " id="main-nav">						
							<ul class="navbar-nav" id="menu">
								<li>
									<a  href="#" class="category-list"><i class="fas fa-list-ul"></i> All Categories</a>
										<ul>
											<li>
												<a href="<?= WEBROOT;  ?>front/product-list.php">All Products</a>
											</li>
											<li>
												<a  href="#">Assecosries</a>
												<ul>
													<li>
														<a href="#">Mobile</a>
													</li>
													<li>
														<a  href="#">Computer</a>
													</li>
													<li>
														<a  href="#">TV</a>
													</li>
													<li>
														<a  href="#">Refrigerator</a>
													</li>
						
												</ul>
											</li>
											<li>
												<a  href="#">Computer</a>
											</li>
											<li>
												<a  href="#">TV & Refrigerator</a>
											</li>
				
										</ul>
								</li>			
							</ul>
						
					</nav>
				</div>
				<!-- Menu Item End -->
				<!-- Search Bar Start -->
				<div class="col-md-8 col-10">
					<div class="menu-two ">
						<nav class="navbar navbar-expand navbar-dark">
							<ul class="navbar-nav">
								<li>
									<a  href="#"> My Account </a>
								</li>
								<li>
									<a  href="<?= WEBROOT; ?>front/cart.php">Cart  <span class="badge badge-danger"><?= $cart->cartProductCount($_SESSION['guest_user']);?></span></a>
								</li>
								<li>
									<a  href=""> Checkout</a>
								</li>		
							</ul>						
					</nav>
					</div>
				</div>
				<!-- Search Bar End -->
			</div>
		</div>
	</div>
	<!-- Menu Bar End -->
