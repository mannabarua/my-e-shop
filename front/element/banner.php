<?php $banners = $banner->all(); ?>

<section>
 	<div class="owl-carousel owl-theme">

 		<?php foreach ($banners as $banner): ?>
		    <div class="item"><img src="<?= IMG ?><?= $banner->picture; ?>" alt=""></div>
		<?php endforeach; ?>

	</div>
 </section>