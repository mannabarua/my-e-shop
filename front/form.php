<!doctype html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title></title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link rel="manifest" href="site.webmanifest">
  <link rel="apple-touch-icon" href="icon.png">
  <!-- Place favicon.ico in the root directory -->

  <link rel="stylesheet" href="css/normalize.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/all.min.css">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="css/owl.carousel.min.css">
  <link rel="stylesheet" href="css/owl.theme.default.min.css">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="css/bootstrap.min.css">
  <link rel="stylesheet" href="css/stellarnav.css">  

  <!-- Main Stylesheet -->
  <link rel="stylesheet" href="css/main.css">
  
</head>

<body>
	<!-- Header Start -->
  <header> 
	<!-- Top Bar Start -->
	<div class="top_bar">
		<div class="container">
			<div class="row align-items-center">
				<!-- Logo -->
				<div class="col-md-4 ">
					<div class="logo mx-auto mx-md-0">
						<a href="index.html"><img src="img/logo.png" alt="logo"  /></a>
					</div>
				</div>
				<!-- Advertise Image top bar -->
				<div class="col-md-8"> 
					<div class="top_search ">
						<div class="input-group">
						  <input type="search" class="form-control" placeholder="Search">
						  <div class="input-group-append">
						    <button class="btn btn-topbar" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button> 
						  </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	  </header>
	<!-- Top Bar End -->
	<!-- Menu Bar Start -->
	<div class="menu_bar sticky-top">
		<div class="container">
			<div class="row align-items-center">
				<!-- Menu Item Start -->
				<div class="col-md-4 col-2">
					<nav class="navbar navbar-expand-md navbar-dark stellarnav " id="main-nav">						
							<ul class="navbar-nav" id="menu">
								<li>
									<a  href="#" class="category-list"><i class="fas fa-list-ul"></i> All Categories</a>
										<ul>
											<li>
												<a href="#">Mobile</a>
											</li>
											<li>
												<a  href="#">Assecosries</a>
												<ul>
													<li>
														<a href="#">Mobile</a>
													</li>
													<li>
														<a  href="#">Computer</a>
													</li>
													<li>
														<a  href="#">TV</a>
													</li>
													<li>
														<a  href="#">Refrigerator</a>
													</li>
						
												</ul>
											</li>
											<li>
												<a  href="#">Computer</a>
											</li>
											<li>
												<a  href="#">TV & Refrigerator</a>
											</li>
				
										</ul>
								</li>			
							</ul>
						
					</nav>
				</div>
				<!-- Menu Item End -->
				<!-- Search Bar Start -->
				<div class="col-md-8 col-10">
					<div class="menu-two ">
						<nav class="navbar navbar-expand navbar-dark">
							<ul class="navbar-nav">
								<li>
									<a  href="#"> My Account </a>
								</li>
								<li>
									<a  href="#">Cart <span class="badge badge-danger">20</span></a>
								</li>
								<li>
									<a  href="#"> Checkout</a>
								</li>		
							</ul>						
					</nav>
					</div>
				</div>
				<!-- Search Bar End -->
			</div>
		</div>
	</div>
	<!-- Menu Bar End -->

<!-- Header End -->


 <!-- Feature Category section Start -->		
 <section>
 	<div class="container">
 		<div class="row">
      <div class="col-12">
        <div class="top_page_link">
          <h2><a href="">Home</a> > <a href="">categroy one</a> > <a href="">product details</a></h2>
        </div>        
      </div>      
 		</div>
 	</div>
 </section>
 <!-- Feature Category section End -->
  
 <!-- Feature Product section Start -->		
 <section>
 	<div class="container">
 		<div class="product-list-section-portion pb-4">
       <form id="contact-form" method="post" action="store.php" role="form">                   
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userName">Full Name</label>
                                <input id="userName"  value="" type="text" name="userName" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userNumber">Mobile Number</label>
                                <input id="userNumber"  value="" type="number" name="userNumber" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userEmail">Email</label>
                                <input id="userEmail"  value="" type="email" name="userEmail" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="userAddress">Address</label>
                                <input id="userAddress"  value="" type="text" name="userAddress" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-danger">Order</button>
                    <!--<input type="submit" class="btn btn-success btn-send" value="Send & Save message">-->
                  </form>
      
    </div>
 	</div>
 </section>
 <!-- Feature Prduct section End -->

   <!-- Footer section Start -->
  <footer id="footer_section">
    <div class="container">
      <div class="row">
          <!-- Footer Logo and text section Start -->
        <div class="col-md-4">
          <div class="footer_logo_portion">
            <img src="img/logo.png" alt="logo">
            <ul>
              <li>
                <a href="#"><i class="fab fa-facebook-f"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-twitter"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-pinterest-p"></i></a>
              </li>
              <li>
                <a href="#"><i class="fab fa-youtube"></i></a>
              </li>
            </ul>            
            <p> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
          </div>
        </div>
          <!-- Footer Logo and text section End -->
          <!-- Footer About Company Menu Start -->
        <div class="col-md-2 offset-md-3">
          <div class="footer_about_portion">
            <h2>About Jiwallet</h2>
            <ul>
              <li>
                <a href="#">About Us</a>
              </li>
              <li>
                <a href="#">News</a>
              </li>
              <li>
                <a href="#">About Security</a>
              </li>
              <li>
                <a href="#">Fees Details</a>
              </li>
              <li>
                <a href="#">Terms & Condition</a>
              </li>              
              <li>
                <a href="#">Privacy Policy</a>
              </li>             
            </ul>
          </div>
        </div>
          <!-- Footer About Company Menu End -->
          <!-- Footer Support Menu Start -->
        <div class="col-md-2 offset-md-1">
          <div class="footer_about_portion">
            <h2>Support</h2>
            <ul>
              <li>
                <a href="#">Contact Us</a>
              </li>
              <li>
                <a href="#">Customer Care</a>
              </li>
              <li>
                <a href="#">FAQ</a>
              </li>
              <li>
                <a href="#">Resource Center</a>
              </li>
              <li>
                <a href="#">Forums</a>
              </li>              
              <li>
                <a href="#">Community</a>
              </li>
             
            </ul>
          </div>
        </div>
          <!-- Footer Support Menu End -->
      </div>
    </div>
  </footer>
  <!-- Footer section End -->
  
  
  <script src="js/vendor/modernizr-3.6.0.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
  <script src="js/plugins.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/stellarnav.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/main.js"></script>

  <!-- Google Analytics: change UA-XXXXX-Y to be your site's ID. -->
  <script>
    window.ga = function () { ga.q.push(arguments) }; ga.q = []; ga.l = +new Date;
    ga('create', 'UA-XXXXX-Y', 'auto'); ga('send', 'pageview')
	


$('#owl-one').owlCarousel({
    loop:true,
    margin:30,
    nav:false,
    autoplay:true,
    autoplayTimeout:2000,
    autoplayHoverPause:true,
    dots:false,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:2
        },
        1000:{
            items:3
        }
    }
})




var owl = $('.owl-carousel');
owl.owlCarousel({
    items:1,
    loop:true,
    margin:10,
    autoplay:true,
    autoplayTimeout:4000,
    autoplayHoverPause:true
});
$('.play').on('click',function(){
    owl.trigger('play.owl.autoplay',[4000])
})
$('.stop').on('click',function(){
    owl.trigger('stop.owl.autoplay')
})

  </script>
  <script src="https://www.google-analytics.com/analytics.js" async defer></script>
</body>

</html>
