<?php  

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/front/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
    
?>
<?php 

    use Eshop\Product\Product;
    $product = new Product();
    if (isset($_GET['id']) && !empty($_GET['id']))
    {
        $id = $_GET['id'];
        $product = $product->show($id);
    }
    else{
        header('location: index.php');
    }

 ?>
<?php ob_start(); ?>

 <!-- Feature Category section Start -->		
 <section>
 	<div class="container">
 		<div class="row">
      <div class="col-12">
        <div class="top_page_link">
          <h2><a href="">Home</a> > <a href="">categroy one</a> > <a href="">product details</a></h2>
        </div>        
      </div>      
 		</div>
 	</div>
 </section>
 <!-- Feature Category section End -->
  
 <!-- Feature Product section Start -->		
 <section>
 	<div class="container">
        <div class="product-list-section-portion">
            <div class="row">
                <div class="col-md-5">
                    <div class="view-product-img">
                        <img src="<?= IMG ?><?= $product->picture; ?>" alt="" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-7">
                    <form action="addTocart.php" method="POST">
                        <input type="hidden" value="<?= $product->product_id; ?>" name="product_id">
                        <input type="hidden" value="<?= $product->picture; ?>" name="picture">
                        <input type="hidden" value="<?= $product->title; ?>" name="product_title">
                        <input type="hidden" value="<?= $product->mrp; ?>" name="mrp">
                        <div class="view-product-details">
                            <h2><?= $product->title; ?></h2>
                            <span><?= $product->mrp; ?>Tk</span>              
                            <del><span> <?= $product->total_sales; ?>Tk</span> </del>             
                            <div class="product-action-box">
                                <input type="number" name="qty" value="1" min="1" class="form-control">
                                <button class="btn btn-danger btn-sm" type="submit">Add to bag</button>
                            </div>
                            <h3>Product Details</h3>
                            <p class="p-1"><?= $product->description; ?></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
 	</div>
 </section>
 <!-- Feature Prduct section End -->
 <?php

$details = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $details, $layout)


 ?>