<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>
      
<div class="p-3">
    <div class="row">
      <div class="col-md-3 col-lg-3 mb-2">
        <div class="card shadow">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs text-custom text-uppercase mb-1">Total</div>
                            <div class="h4 mb-0 text-gray-800">150</div>
                        </div>
                        <div class="col-auto bg-custom p-3 rounded-circle shadow">
                            <i class="fas fa-users fa-2x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="col-md-3 col-lg-3 mb-2">
        <div class="card shadow">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs text-custom text-uppercase mb-1">Total</div>
                            <div class="h4 mb-0 text-gray-800">150</div>
                        </div>
                        <div class="col-auto bg-custom p-3 rounded-circle shadow">
                            <i class="fa fa-users fa-2x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="col-md-3 col-lg-3 mb-2">
        <div class="card shadow">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs text-custom text-uppercase mb-1">Total</div>
                            <div class="h4 mb-0 text-gray-800">150</div>
                        </div>
                        <div class="col-auto bg-custom p-3 rounded-circle shadow">
                            <i class="fas fa-users fa-2x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="col-md-3 col-lg-3 mb-2">
        <div class="card shadow">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs text-custom text-uppercase mb-1">Total</div>
                            <div class="h4 mb-0 font-weight-bold text-gray-800">150</div>
                        </div>
                        <div class="col-auto bg-custom p-3 rounded-circle shadow">
                            <i class="fas fa-users fa-2x text-white"></i>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    </div>
</div>


<?php 

  $index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $index, $layout)

 ?>