
<?php

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();

?>

<?php ob_start(); ?>

<?php

    use Eshop\Product\Product;
    use Eshop\Utility\Messages;

    $product = new Product();
    $products = $product->all();

 ?>
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View Product</h2>
              </div>
              <div class="col-4">
                <a href ="add-product.php" class="main-button" >Add New</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-table">

                    <?php if ($messages = Messages::get()):  ?>
                        <div class="alert alert-success"><?= $messages; ?></div>
                    <?php endif; ?>

                  <table class="table table-striped">
                    <thead>
                      <tr>
                        <th width="30%">Title</th>
                        <th width="10%">Picture</th>
                        <th width="50%">Short description</th>
                        <th width="10%">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php

                        foreach ($products as $product):

                    ?>
                        <tr>
                            <td><a href="#"> <?= $product->title; ?></a> </td>
                            <td> 
                                <img src="<?= IMG.$product->picture; ?>" alt="image" width="100" height="60">
                            </td>
                            <td>
                                <?= $product->short_description; ?>
                            </td>
                            <td>
                                <a class="btn btn-primary btn-sm text-white" href="show.php?id=<?= $product->product_id; ?>"><i class="far fa-eye"></i></a>
                                <a class="btn btn-success btn-sm text-white" href="edit.php?id=<?= $product->product_id; ?>"><i class="far fa-edit"></i></a>
                                <form action="delete.php" method="POST">
                                    <input type="hidden" value="<?= $product->product_id; ?>" name="deleteId">
                                    <button class="btn btn-sm btn-danger text-white" onclick="javascript: return confirm('Are you confirm delete record?')"><i class="far fa-trash-alt"></i></button>
                                </form>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>


<?php 

  $index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $index, $layout)

 ?>