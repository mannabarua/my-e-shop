<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
?>
<?php
    use Eshop\Product\Product;
    use Eshop\Utility\Messages;
    $product = new Product();
    ob_start();
 ?>

<?php
    if (isset($_GET['id']) && !empty($_GET['id']))
    {
        $id = $_GET['id'];
        $products = $product->show($id);
    }
    else{
        header('location: index.php');
    }


?>

    
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2>Update Product</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" enctype="multipart/form-data" action="update.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <input id="id"  value="<?= $products->product_id;?>" type="hidden" name="product_id" class="form-control sr-only">
                        <input id="id"  value="<?= $products->picture;?>" type="hidden" name="picture" class="form-control sr-only">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"  value="<?= $products->title;?>" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">Picture</label>
                                <input id="picture"  value="<?= $products->picture;?>" type="file" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="short_description">Short Description</label>
                                <input id="short_description"  value="<?= $products->short_description;?>" type="text" name="short_description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input id="description"  value="<?= $products->description;?>" type="text" name="description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="total_sales">Total_sales</label>
                                <input id="total_sales"  value="<?= $products->total_sales;?>" type="text" name="total_sales" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Product_Image">Product Image</label>
                                <img src="<?= IMG . $products->picture;?>" width="100" alt="">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="Cost">Cost</label>
                                <input id="Cost"  value="<?=$products->cost; ?>" type="text" name="cost" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="MRP">MRP</label>
                                <input id="MRP"  value="<?=$products->mrp; ?>" type="text" name="mrp" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <?php 
                                    if ($products->is_draft == 1 ) {
                                        $checkbox = 'checked';
                                    }else{
                                        $checkbox = '';
                                    }
                                 ?>
                                <input id="isDraft" <?= $checkbox ; ?> value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isactive">Is Active</label>
                                <?php 
                                    if ($products->is_active == 1 ) {
                                        $checkbox = 'checked';
                                    }else{
                                        $checkbox = '';
                                    }
                                 ?>
                                <input id="isactive" <?= $checkbox ; ?>  value="1" type="checkbox" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" name="update" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       
<?php 

  $edit_product = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $edit_product, $layout)

 ?>