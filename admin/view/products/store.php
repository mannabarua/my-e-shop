<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php

	use Eshop\Product\Product;
	use Eshop\Utility\Messages;
	$product = new Product();
	if (isset($_POST)&& !empty($_POST))
	{
		$imgPath 	= DOCROOT.'/eshop/img/';
		$data 		= $_POST;
		$ImgName  	= $_FILES['picture']['name'];
		$ImageTmp 	= $_FILES['picture']['tmp_name'];

		$imgExt 	= pathinfo($ImgName, PATHINFO_EXTENSION);
		$data['picture'] = uniqid().'.'.$imgExt;

		if ($product->store($data)) 
		{
			move_uploaded_file($ImageTmp, $imgPath.$data['picture']);
			Messages::set('Product has been added successfully');
			header("location: index.php");
		}
		else
		{
			Messages::set('Sorry!.. There is a problem. Please try again');
			header("location: add-product.php");
		}
		
	}
	else{
		header("location: add-product.php");
	}


?>

