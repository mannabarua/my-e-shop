<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Page\Page;
    use Eshop\Utility\Messages;
    $page = new Page();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$page->softDelete($id);
	}
	else
	{
		header('location: index.php');
	}

?>