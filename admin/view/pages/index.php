
<?php

    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();

?>

<?php ob_start(); ?>

<?php

    use Eshop\Page\Page;
    use Eshop\Utility\Messages;

    $page = new Page();
    $pages = $page->all();

 ?>
 <form action="quickAccess.php" method="POST">
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View page</h2>
              </div>
              <div class="col-4">
                <a href ="add-page.php" class="main-button" >Add New</a>
              </div>
            </div>
            <label for="select">Check Status</label>
            <div class="row">
                <div class="col-md-3">
                    <select class="form-control" name="" id="select">
                        <option>Select one</option>
                        <option value="delete">Is New</option>
                        <option value="">Soft Delete</option>
                        <option value="">Is Draft</option>
                        <option value="">Is Active</option>
                    </select>
                </div>
                <div class="col-md-2">
                    <button type="submit" name="tatus" class="btn btn-info">Apply</button>
                </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-table">

                    <?php if ($messages = Messages::get()):  ?>
                        <div class="alert alert-success"><?= $messages; ?></div>
                    <?php endif; ?>

                  <table class="table table-striped" id="dataTable">
                    <div class="custom-control custom-checkbox m-1">
                        <input class="custom-control-input"  type="checkbox" id="selectAllBoxes">
                        <label class="checkbox custom-control-label text-muted" for="selectAllBoxes">
                        Select All 
                        </label>
                    </div>
                    <thead>
                      <tr>
                        <th>SL</th>
                        <th>Title</th>
                        <th>Picture</th>
                        <th>link</th>
                          <th> Description</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i = 0;
                        foreach ($pages as $page):
                            $i++;
                    ?>
                        <tr>
                            <td><input type="checkbox" name="" class="checkbox" value="<?= $page->page_id; ?>"> <?=$i;?></td>
                            <td><a href="#"> <?= $page->title; ?></a> </td>
                            <td> 
                                <img src="<?= IMG.$page->picture; ?>" alt="image" width="100" height="60">
                            </td>
                            <td>
                                <?= $page->link; ?>
                            </td>
                            <td>
                                <?= $page->description; ?>
                            </td>
                            <td>
                                <a title="View" class="btn btn-primary btn-sm text-white" href="show.php?id=<?= $page->page_id; ?>"><i class="far fa-eye"></i></a>
                                <a title="Edit" class="btn btn-success btn-sm text-white" href="edit.php?id=<?= $page->page_id; ?>"><i class="far fa-edit"></i></a>
                                <a title="Soft Delete" class="btn btn-danger btn-sm text-white" href="softDelete.php?id=<?= $page->page_id; ?>"><i class="far fa-trash-alt"></i></a>
                            </td>
                        </tr>
                    <?php
                    endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
</form>

<?php 

  $index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $index, $layout)

 ?>