<?php 

$bannerId = $_GET['id'];

$servername = "localhost";
$username = "root";
$password = "";
$dbname ="jishop";

// Connecting database
$conn = new PDO("mysql:host=$servername;dbname=$dbname;", $username, $password);
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


// Selecting query
$query = "DELETE FROM `banners` WHERE `banners`.`id` = :id";

$sth = $conn->prepare($query);
$sth -> bindparam(':id', $bannerId);
$result = $sth->execute();
// redirect the page

header("location:index.php");