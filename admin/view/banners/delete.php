<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Product\Product;
    use Eshop\Utility\Messages;
    $product = new Product();
	
	if (isset($_POST['deleteId']) && !empty($_POST['deleteId'])) {
		$id = $_POST['deleteId'];
		$delete = $product->delete($id);
	}else{
		header('location: index.php');
	}

?>