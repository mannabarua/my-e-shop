<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Category\Category;
    use Eshop\Utility\Messages;
    $categories = new Category();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$categories->softDelete($id);
	}
	else
	{
		header('location: index.php');
	}

?>