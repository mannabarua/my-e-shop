<?php

	include_once($_SERVER['DOCUMENT_ROOT'].'/eshop/bootstrap.php');
    use Eshop\Category\Category;
    use Eshop\Utility\Messages;

    $category = new Category();

    if (isset($_POST) && !empty($_POST)) 
    {
        $data	= $_POST;
        $category->update($data);
    }
    else
    {
        header('location: index.php');
    }


?>