<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
ob_start();
include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
$layout = ob_get_contents();
ob_end_clean();
?>
<?php
ob_start();
?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Add New</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="contactName">Name</label>
                                <input id="contactName"  value="" type="text" name="contactName" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="contactAddress">Address</label>
                                <input id="contactAddress"  value="" type="text" name="contactAddress" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="contactNumber">Phone Number</label>
                                <input id="contactNumber"  value="" type="number" name="contactNumber" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="contactEmail">Email</label>
                                <input id="contactEmail"  value="" type="email" name="contactEmail" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="comment">Comment</label>
                                <textarea class="form-control" name="comment" id="comment"></textarea>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

<?php

$add_contact = ob_get_contents();
ob_end_clean();
echo str_replace("##MAIN_CONTENT##", $add_contact, $layout)

?>