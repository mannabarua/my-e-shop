<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
?>

<?php  ob_start(); ?>

<?php
    use Eshop\Brand\Brand;
    use Eshop\Utility\Messages;
    $brand = new Brand();

    if (isset($_GET['id']) && !empty($_GET['id']))
    {
        $id = $_GET['id'];
        $brand = $brand->show($id);
    }
    else{
        header('location: index.php');
    }
?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Edit Brand</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="update.php" role="form">
                    <input type="hidden" name="id" value="<?= $brand->brand_id; ?>">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"  value="<?= $brand->title; ?>" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input id="link"  value="<?= $brand->link; ?>" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <?php 
                                    if ($brand->is_draft == 1 ) {
                                        $checkbox = 'checked';
                                    }else{
                                        $checkbox = '';
                                    }
                                 ?>
                                <input id="isDraft" <?= $checkbox; ?>  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isactive">Is Active</label>
                                <?php 
                                    if ($brand->is_active == 1 ) {
                                        $checkbox = 'checked';
                                    }else{
                                        $checkbox = '';
                                    }
                                 ?>
                                <input id="isactive" <?= $checkbox; ?> value="1" type="checkbox" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

     
<?php 

    $add_brand = ob_get_contents();
    ob_end_clean();
    echo str_replace("##MAIN_CONTENT##", $add_brand, $layout)

?>