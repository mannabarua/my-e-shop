<?php
  ob_start();
  include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
  $layout = ob_get_contents();
  ob_end_clean();
?>

<?php 
  ob_start();
 ?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Add Brand</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input id="title"  value="" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="link">Link</label>
                                <input id="link"  value="" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isDraft">Is Draft</label>
                                <input id="isDraft"  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="isactive">Is Active</label>
                                <input id="isactive"  value="1" type="checkbox" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
     
 <?php 

  $add_brand = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $add_brand, $layout)

 ?>