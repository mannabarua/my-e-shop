<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Brand\Brand;
    use Eshop\Utility\Messages;
    $brand = new Brand();
	
	
	if (isset($_GET['id']) && !empty($_GET['id'])) 
	{
		$id = $_GET['id'];
		$brand->restore($id);
	}
	else
	{
		header('location: trash.php');
	}

?>