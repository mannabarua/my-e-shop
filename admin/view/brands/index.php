<?php $conn = new PDO("mysql:host=localhost;dbname=laracom","root",""); ?>

<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
?>

<?php ob_start(); ?>
<?php 

    use Eshop\Brand\Brand;
    use Eshop\Utility\Messages;

    $brand = new Brand();
    $brands = $brand->all();


 ?>     
        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View Brands</h2>
              </div>
              <div class="col-4">
                <a href ="add-brand.php" class="main-button" >Add New</a>
              </div>
            </div>
            <?php if ($messages = Messages::get()):  ?>
                <div class="alert alert-success"><?= $messages; ?></div>
            <?php endif; ?>
            <div class="row">
              <div class="col-12">
                <div class="table-responsive">
                  <table id="dataTable" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Link</th>
                        <th>Is Active</th>
                        <th>Is Draft</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php
                        
                        foreach ($brands as $brand):
                    ?>
                        <tr>
                          <td><a href="#"> <?= $brand->title; ?></a> </td>
                          <td><a href=""><?= $brand->link; ?></a></td>
                          <td>
                              <?php 
                                    if ($brand->is_draft == 1) {
                                        echo '<sapn  class="text-info"><i class="far fa-check-circle"></i> Yes<sapn>';
                                    }else{
                                        echo '<sapn class="text-danger"><i class="far fa-times-circle"></i> No<sapn>';
                                    }
                                 ?>
                          </td>
                          <td>
                             <?php 
                                if ($brand->is_active == 1) {
                                    echo '<sapn  class="text-info"><i class="far fa-check-circle"></i> Yes<sapn>';
                                }else{
                                    echo '<sapn class="text-danger"><i class="far fa-times-circle"></i> No<sapn>';
                                }
                             ?>
                          </td>
                          <td><a title="View" href="show.php?id=<?= $brand->brand_id; ?>"><i class="far fa-eye"></i></a> | 
                            <a title="Edit" href="edit.php?id=<?= $brand->brand_id; ?>"><i class="far fa-edit"></i></a> | 
                            <a title="Soft Delete" onclick="javascript: return confirm('Are you confirm delete record?')" href="softDelete.php?id=<?= $brand->brand_id; ?>"><i class="far fa-trash-alt"></i></a></td>
                        </tr>
                    <?php
                        endforeach;
                    ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div> 
        
<?php 

  $brands_index = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $brands_index, $layout)

 ?>