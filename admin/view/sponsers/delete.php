<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Sponser\Sponser;
    use Eshop\Utility\Messages;
    $sponser = new Sponser();
	
	if (isset($_POST['deleteId']) && !empty($_POST['deleteId'])) {
		$id = $_POST['deleteId'];
		$delete = $sponser->delete($id);
	}else{
		header('location: index.php');
	}

?>