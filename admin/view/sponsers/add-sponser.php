<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
    ?>



<?php 
    use Eshop\Utility\Messages;
    ob_start();
    
 ?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2> View sponser</h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >View sponsers</a>
              </div>
            </div>
                <?php if ($massages = Messages::get()):  ?>
                    <div class="alert alert-success"><?=  $massages; ?></div>
                <?php endif; ?>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" enctype="multipart/form-data" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="id">ID</label>-->
<!--                                <input id="id"  value="" type="text" name="id" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="brand_id">brand_id</label>-->
<!--                                <input id="brand_id"  value="" type="text" name="brand_id" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="label_id">label_id</label>-->
<!--                                <input id="label_id"  value="" type="text" name="label_id" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">title</label>
                                <input id="title"  value="" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">picture</label>
                                <input id="picture"  value="" type="file" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="link">link</label>
                                <input id="link"  value="" type="text" name="link" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="promotional_message">promotional_message</label>
                                <input id="promotional_message"  value="" type="text" name="promotional_message" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="html_banner">html_banner</label>
                                <input id="html_banner"  value="" type="text" name="html_banner" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="sponser_type">sponser_type</label>-->
<!--                                <input id="sponser_type"  value="" type="text" name="sponser_type" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="is_new">is_new</label>-->
<!--                                <input id="is_new"  value="" type="text" name="is_new" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="cost">cost</label>-->
<!--                                <input id="cost"  value="" type="text" name="cost" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="mrp">mrp</label>-->
<!--                                <input id="mrp"  value="" type="text" name="mrp" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                        <div class="col-lg-6">-->
<!--                            <div class="form-group">-->
<!--                                <label for="special_price">special_price</label>-->
<!--                                <input id="special_price"  value="" type="text" name="special_price" class="form-control">-->
<!--                                <div class="help-block with-errors"></div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="col-lg-6">
                           <div class="form-group">
                                <label for="soft_delete">soft_delete</label>
                                <input id="soft_delete"  value="" type="text" name="soft_delete" class="form-control">
                               <div class="help-block with-errors"></div>
                           </div>
                              </div>
                        <div class="col-lg-6">
                           <div class="form-group">
                                <label for="is_draft">is_draft</label>
                                <input id="is_draft"  value="" type="text" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                           </div>
                        </div>
                       <div class="col-lg-6">
                            <div class="form-group">
                               <label for="is_active">is_active</label>
                                <input id="is_active"  value="" type="text" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                           </div>
                       </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="created_at">created_at</label>
                                <input id="created_at"  value="" type="text" name="created_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="modified_at">modified_at</label>
                                <input id="modified_at"  value="" type="text" name="modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" name="store" class="btn btn-info">Save</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       
<?php 

  $add_sponser = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $add_sponser, $layout)

 ?>