<?php
    ob_start();
    include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/admin/view/layout/index.php');
    $layout = ob_get_contents();
    ob_end_clean();
?>
<?php 
    use Eshop\Pponser\Pponser;
    $sponsers = new Sponser();
    ob_start();
 ?>
<?php
    if (empty($_GET['id'])) {
        header('location: index.php');
    }
    $id = $_GET['id'];
    $sponser = $sponsers->show($id);
?>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-8">
                <h2 class="display-5"> View sponser</h2>
              </div>
              <div class="col-4">
                <a href ="index.php" class="main-button" >Go Back</a>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="view-details">
                  <img src="<?= IMG . $sponser->picture;?>" alt="banner" width="40%" height="50%">
                  <h2><?= $sponser->title;?></h2>
                  <p><?= $sponser->promotional_message;?></p>
                  <p class="text-dark">Is Active: <span>Yes</span></p>
                </div>
              </div>
            </div>
          </div>
        </div>
  
        
<?php 

  $edit_sponser = ob_get_contents();
  ob_end_clean();
  echo str_replace("##MAIN_CONTENT##", $edit_sponser, $layout)

 ?>