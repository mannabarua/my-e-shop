<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
      <link rel="manifest" href="site.webmanifest">
      <link rel="apple-touch-icon" href="icon.png">
      <!-- Place favicon.ico in the root directory -->
      <!-- Font Awesome -->
      <link rel="stylesheet" href="<?=CSS?>fontawesome/css/all.min.css">

      <!-- Bootstrap -->
      <link rel="stylesheet" href="<?=CSS?>bootstrap.min.css">
      <link rel="stylesheet" href="<?=CSS?>/datatable/dataTables.bootstrap4.min.css">

      <!-- Main Stylesheet -->
      <link rel="stylesheet" href="<?=CSS?>main.css"> 


    <title>Admin Panel</title>
  </head>
  <body>
   
   <div class="body-wrapper"> <!-- Full body wrap -->

     	<aside id="sidebar"> <!-- Sidebar Start  -->
        <div class="sidebar-body">
       		<div class="sidebar-header">
       			<div class="logo-section">
              <img src="<?=IMG?>logo-white.png" alt="" style="width:140px;">
            </div>
            <!-- admin Info -->
            <div class="admin-info ">
                <div class="admin-image">
                    <img src="<?=IMG?>jishat.jpg" width="48px" height="48px" alt="User" />
                </div>
                <div class="info-container">
                    <h2>AR Jishat</h2>
                    <h3>arjishat@gmail.com</h3>
                </div>
            </div>
       		</div>   		
       		<nav class="sidebar-menu">
                <ul class="list-unstyled ">
                    <h2>MAIN NAVIGATION</h2>
                    <li>
                        <a href="<?=DESHBOARD.'index.php'?>"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-toggle="collapse" class="dropDownMenu" data-target="#subMenuOne" aria-expanded="false"> <i class="fas fa-tachometer-alt"></i> Products</a>
                        <ul  id="subMenuOne" class="collapse list-unstyled">
                            <li>
                                <a href="<?=PRODUCTS.'add-product.php'?>">Add Products</a>
                            </li>
                            <li>
                                <a href="<?=PRODUCTS.'index.php'?>">View Products</a>
                            </li>
                            <li>
                                <a href="<?=PRODUCTS.'trash.php'?>">Trash Products</a>
                            </li>
                        </ul> 
                    </li>

                    <li>
                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#banner"> <i class="fas fa-tachometer-alt"></i> Brands</a>
                        <ul class="collapse list-unstyled" id="banner">
                            <li>
                                <a href="<?=BRANDS.'add-brand.php'?>">Add Brand</a>
                            </li>
                            <li>
                                <a href="<?=BRANDS.'index.php'?>">View Brand</a>
                            </li>
                            <li>
                                <a href="<?=BRANDS.'trash.php'?>">Trash Brand</a>
                            </li>
                        </ul>
                    </li>
                    <!-- <li>
                        <a href="#"><i class="fas fa-tachometer-alt"></i> Categories</a>
                    </li> -->
                    <li>
                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#Categories"> <i class="fas fa-tachometer-alt"></i> Categories</a>
                        <ul class="collapse list-unstyled" id="Categories">
                            <li>
                                <a href="<?=CATEGORY.'add-category.php'?>">Add Category</a>
                            </li>
                            <li>
                                <a href="<?=CATEGORY.'index.php'?>">View Category</a>
                            </li>
                            <li>
                                <a href="<?=CATEGORY.'trash.php'?>">Trash Category</a>
                            </li>
                        </ul> 
                    </li>
                    <li>
                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#Banner"> <i class="fas fa-tachometer-alt"></i> Banner</a>
                        <ul class="collapse list-unstyled" id="Banner">
                            <li>
                                <a href="<?= BANNER .'add-banner.php'?>">Add Banner</a>
                            </li>
                            <li>
                                <a href="<?= BANNER .'index.php'?>">View Banner</a>
                            </li>
                        </ul> 
                    </li>
<!--                    <li>-->
<!--                        <a href="javascript:void(0)" data-toggle="collapse" data-target="#Pages"> <i class="fas fa-tachometer-alt"></i> Pages</a>-->
<!--                        <ul class="collapse list-unstyled" id="Pages">-->
<!--                            <li>-->
<!--                                <a href="--><?//=PAGE.'create.php'?><!--">Add page</a>-->
<!--                            </li>-->
<!--                            <li>-->
<!--                                <a href="--><?//=PAGE.'index.php'?><!--">View page</a>-->
<!--                            </li>-->
<!--                        </ul> -->
<!--                    </li>-->
              </ul> 
          </nav>       		
        </div>

        <div class="sidebar-button">
          <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-align-justify"></i> <span></span>
          </button>  
        </div>        
     	</aside>
      
      
   	<!-- Sidebar Start  End-->
       <div id="body-content"> <!-- body content start -->