   	  </div><!-- body content End -->   	
   	
  </div> <!-- Full body wrap ENd-->
	<!-- Logout Modal-->
	<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog" role="document">
	        <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
	                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">×</span>
	                </button>
	            </div>
	            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
	            <div class="modal-footer">
	                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
	                <a class="btn btn-primary" href="login.html">Logout</a>
	            </div>
	        </div>
	    </div>
	</div>  

    <!--  JavaScript link -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="<?=JS?>bootstrap.min.js"></script>
    <script src="<?=JS?>jquery.easing.min.js"></script>
    <!-- data table -->
    <script src="<?=JS?>datatables/jquery.dataTables.min.js"></script>
    <script src="<?=JS?>datatables/dataTables.bootstrap4.min.js"></script>
    <script src="<?=JS?>datatables/dataTables-demo.js"></script>
    <script src="<?=JS?>main.js"></script>

    
    
    
  </body>
</html>