<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php
	use Eshop\Tag\Tag;
    use Eshop\Utility\Messages;
    $tag = new Tag();
	
	if (isset($_POST['deleteId']) && !empty($_POST['deleteId'])) {
		$id = $_POST['deleteId'];
		$delete = $tag->delete($id);
	}else{
		header('location: index.php');
	}

?>