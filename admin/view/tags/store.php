<?php include_once($_SERVER["DOCUMENT_ROOT"].'/eshop/bootstrap.php'); ?>

<?php

	use Eshop\Tag\Tag;
	use Eshop\Utility\Messages;
	$tag = new Tag();
	if (isset($_POST)&& !empty($_POST))
	{

		$imgPath 	= DOCROOT.'/eshop/img/';
		$data 		= $_POST;
		$ImgName  	= $_FILES['picture']['name'];
		$ImageTmp 	= $_FILES['picture']['tmp_name'];

		$imgExt 	= pathinfo($ImgName, PATHINFO_EXTENSION);
		$data['picture'] = uniqid().'.'.$imgExt;

		if ($tag->store($data)) 
		{
			move_uploaded_file($ImageTmp, $imgPath.$data['picture']);
			Messages::set('tag has been added successfully');
			header("location: index.php");
		}
		else
		{
			Messages::set('Sorry!.. There is a problem. Please try again');
			header("location: add-contact.php");
		}
		
	}
	else{
		header("location: add-contact.php");
	}


?>

