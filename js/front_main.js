//menu active

jQuery('#main-nav').stellarNav({

  // adds default color to nav. (light, dark)
  theme     : 'dark',

  // number in pixels to determine when the nav should turn mobile friendly
  breakpoint: 769,

  // label for the mobile nav
  menuLabel: '',

  // adds a click-to-call phone link to the top of menu - i.e.: "18009084500"
  phoneBtn: false,

  // adds a location link to the top of menu - i.e.: "/location/", "http://site.com/contact-us/"
  locationBtn: false,

  // makes nav sticky on scroll
  sticky     : false,
  
  // how fast the dropdown should open in milliseconds
  openingSpeed: 250,

  // controls how long the dropdowns stay open for in milliseconds
  closingDelay: 250,

  // 'static', 'top', 'left', 'right'
  position: 'right',

  // shows dropdown arrows next to the items that have sub menus
  showArrows: true,

  // adds a close button to the end of nav
  closeBtn     : false,

  // fixes horizontal scrollbar issue on very long navs
  scrollbarFix: false,
  
  // enables mobile mode
  mobileMode: false

});

//Menu End