<?php

namespace Eshop\Sponser;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Sponser
{
	public $conn;
	public $sponser_id;
	public $title;
	public $picture;
	public $link;
	public $promotional_message;
	public $html_banner;
	public $sponser_type;
	public $is_new;
	public $cost;
	public $mrp;
	public $special_price;
	public $soft_delete;
	public $is_draft;
	public $is_active;
	public $created_at;
	public $modified_at;

	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  sponsers WHERE soft_delete = 0 ORDER BY sponser_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $sponsers =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $sponsers;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO `sponsers`(`title`, `picture`, `link`, `promotional_message`, `html_banner`,`is_active`,`is_draft`,`created_at`,`modified_at`) VALUES (:title, :picture, :link, :promotional_message,:html_banner,:is_active,:is_draft,:created_at,:modified_at)";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":link",$this->link);
		$stmt->bindparam(":promotional_message",$this->promotional_message);
		$stmt->bindparam(":html_banner",$this->html_banner);
        $stmt->bindparam(":is_active",$this->is_active);
        $stmt->bindparam(":is_draft",$this->is_draft);
        $stmt->bindparam(":created_at",$this->created_at);
        $stmt->bindparam(":modified__at",$this->modified__at);
		$sponser = $stmt->execute();
		return $sponser;
    }
    public function build($data)
    {
    	$this->sponser_id 		= $data['sponser_id'];
    	$this->title 			= $data['title'];
    	$this->link= $data['link'];
    	$this->promotional_message 		= $data['promotional_message'];
    	$this->html_banner 		= $data['html_banner'];
    	$this->picture			= $data['picture'];
        $this->is_active		= $data['is_active'];
        $this->is_draft			= $data['is_draft'];
        $this->created_at		= $data['created_at'];
        $this->modified__at			= $data['modified__at'];
    	// $this->sponser_type 	= $data['sponser_type'];
    	// $this->is_new 			= $data['is_new'];
    	// $this->cost 			= $data['cost'];
    	// $this->mrp 				= $data['mrp'];
    	// $this->special_price 	= $data['special_price'];
    	// $this->is_active 		= $data['is_active'];
    	// $this->created_at 		= $data['created_at'];
    	// $this->modified_at 		= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  sponsers WHERE sponser_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $sponser =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$sponser) 
        {
        	Messages::set('Sorry!.. sponser is not found');
        	header('location: index.php');
        }
        return $sponser;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM sponsers WHERE sponser_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->picture);

		$query = "DELETE FROM sponsers WHERE sponser_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('sponser has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE sponsers SET soft_delete = 1 WHERE sponser_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("sponser remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> sponser? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  sponsers WHERE soft_delete = 1 ORDER BY sponser_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE sponsers SET soft_delete = 0 WHERE sponser_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('sponser restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `sponsers` SET `title`= :title,`picture`= :picture, ";
	    $query .= "`link`=:link,`promotional_message`=:promotional_message, ";
	    $query .= "`html_banner`=:html_banner WHERE sponser_id = :id";
	    $result = $this->conn->prepare($query);
	    $result->bindparam(":title",$this->title);
	    $result->bindparam(":picture",$this->picture);
	    $result->bindparam(":link",$this->link);
	    $result->bindparam(":promotional_message",$this->promotional_message);
	    $result->bindparam(":html_banner",$this->html_banner);
        $result->bindparam(":is_draft",$this->is_draft);
        $result->bindparam(":created_at",$this->created_at);
        $result->bindparam(":is_active",$this->is_active);
        $result->bindparam(":modified__at",$this->modified__at);
	    $result->bindparam(":id",$this->sponser_id);
	    
	    $update =  $result->execute();
	    if ($update) {
	    	Messages::set('sponser has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}