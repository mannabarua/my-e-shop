<?php

namespace Eshop\Brand;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Brand
{
	public $conn;
	public $brand_id;
	public $title;
	public $link;
	public $is_draft;
	public $is_active;
	public $soft_delete;
	public $created_at;
	public $modified_at;




	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  brands WHERE soft_delete = 0 ORDER BY brand_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $brands =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $brands;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO brands(title, link, is_draft, is_active, created_at, modified_at) 
    			 VALUES (:title, :link, :is_draft, :is_active, now(), now())";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":link",$this->link);
		$stmt->bindparam(":is_draft",$this->is_draft);
		$stmt->bindparam(":is_active",$this->is_active);
		$brand = $stmt->execute();

		if ($brand) 
		{
			Messages::set('has been added successfully');
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem. Please try again');
        	header('location: add-brand.php');
		}
		return $delete;
    }
    public function build($data)
    {
    	$this->brand_id 	= $data['brand_id'];
    	$this->title 		= $data['title'];
    	$this->link 		= $data['link'];
		$this->is_draft 	= empty($data['is_draft']) ? 0 : $data['is_draft'];
    	$this->is_active 	= empty($data['is_active']) ? 0 : $data['is_active'];
    	$this->soft_delete	= $data['soft_delete'];
    	$this->created_at 	= $data['created_at'];
    	$this->modified_at 	= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  brands WHERE brand_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $product =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$product) 
        {
        	Messages::set('Sorry!.. Product is not found');
        	header('location: index.php');
        }
        return $product;
    }

    public function delete($id)
    {

		$query = "DELETE FROM brands WHERE brand_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('Product has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE brands SET soft_delete = 1 WHERE brand_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("Product remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> product? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  brands WHERE soft_delete = 1 ORDER BY brand_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE brands SET soft_delete = 0 WHERE brand_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('Product restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `brands` SET `title`= :title,`link`= :link, is_draft = :is_draft, is_active = :is_active, modified_at = now()  WHERE brand_id = :id";
	    $stmt = $this->conn->prepare($query);
	    $stmt->bindparam(":title",$this->title);
	    $stmt->bindparam(":link",$this->link);
	    $stmt->bindparam(":is_draft",$this->is_draft);
	    $stmt->bindparam(":is_active",$this->is_active);
	    $stmt->bindparam(":id",$this->brand_id);
	    $update = $stmt->execute();
	    if ($update) {
	    	Messages::set('Brand has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}