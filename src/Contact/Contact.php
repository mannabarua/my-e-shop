<?php

namespace Eshop\Contact;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class contact
{
	public $conn;
	public $contact_id;
	public $name;
	public $email;
	public $phoneNumber;
	public $address;
	public $comment;
    public $status;
    public $created_at;
	public $soft_delete;

	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  contacts WHERE soft_delete = 0 ORDER BY contact_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $contacts =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $contacts;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO contact(name, email, phoneNumber, address, comment, created_at) VALUES (:name, :email, :phoneNumber, :address, :comment, :created_at)";
		$stmt = $this->conn->prepare($query);
		$stmt -> bindparam(':name',$this->name);
		$stmt -> bindparam(':email',$this->email);
		$stmt -> bindparam(':phoneNumber',$this->phoneNumber);
		$stmt -> bindparam(':address', $this->address);
		$stmt -> bindparam(':comment', $this->comment);
		$stmt -> bindparam(':created_at',$this->created_at );
		$contact = $stmt->execute();
		if ($contact) 
        {
        	Messages::set('Record insert successfully');
        	header('location: index.php');
        }
        else 
        {
        	Messages::set('Sorry!.. There is a problem');
        	header('location: create.php');
        }
		return $contact;
    }
    public function build($data)
    {
    	$this->contact_id 	= $data['contact_id'];
    	$this->name 		= $data['contactName'];
    	$this->email 		= $data['contactEmail'];
    	$this->phoneNumber 	= $data['contactNumber'];
    	$this->address 		= $data['contactAddress'];
    	$this->comment		= $data['comment'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  contacts WHERE contact_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $contact =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$contact) 
        {
        	Messages::set('Sorry!.. contact is not found');
        	header('location: index.php');
        }
        return $contact;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM contacts WHERE contact_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->email);

		$query = "DELETE FROM contacts WHERE contact_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('contact has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE contacts SET soft_delete = 1 WHERE contact_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("contact remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> contact? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  contacts WHERE soft_delete = 1 ORDER BY contact_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE contacts SET soft_delete = 0 WHERE contact_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('contact restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `contacts` SET `name`= :name,`email`= :email, ";
	    $query .= "`subject`=:subject,`comment`=:comment, ";
	    $query .= "`status`=:status WHERE contact_id = :id";
	    $result = $this->conn->prepare($query);
	    $result->bindparam(":name",$this->name);
	    $result->bindparam(":email",$this->email);
	    $result->bindparam(":subject",$this->subject);
	    $result->bindparam(":comment",$this->comment);
	    $result->bindparam(":status",$this->status);
	    $result->bindparam(":id",$this->contact_id);
	    $update =  $result->execute();
	    if ($update) {
	    	Messages::set('contact has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}