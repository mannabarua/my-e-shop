<?php 

namespace Eshop\Cart;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Cart
{
	public $conn;
	public $cart_id;
	public $sid;
	public $product_id;
	public $picture;
	public $product_title;
	public $qty;
	public $unite_price;
	public $total_price;
	
	function __construct(){
		$this->conn = Db::connect();
	}

	public function store($data)
    {
    	$this->build($data);

    	// echo "<pre>";
    	// var_dump($data);
    	// die();
    	$query = "INSERT INTO carts(sid, product_id, picture, product_title, qty, unite_price, total_price) VALUES (:sid, :product_id, :picture, :product_title, :qty, :unite_price, :total_price)";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":sid",$this->sid);
		$stmt->bindparam(":product_id",$this->product_id);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":product_title",$this->product_title);
		$stmt->bindparam(":qty",$this->qty);
		$stmt->bindparam(":unite_price",$this->unite_price);
		$stmt->bindparam(":total_price",$this->total_price);
		$cart = $stmt->execute();

		if ($cart) 
		{
			Messages::set('Product has been added successfully');
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
		return $cart;
    }

    public function build($data)
    {
    	$this->cart_id 			= $data['cart_id'];
    	$this->sid 				= $data['sid'];
    	$this->product_id 		= $data['product_id'];
    	$this->picture 			= $data['picture'];
    	$this->product_title	= $data['product_title'];
    	$this->qty				= $data['qty'];
    	$this->unite_price 		= $data['unite_price'];
    	$this->total_price 		= $data['total_price'];
    }

    public function all($sid)
    {
    	$query = "SELECT * FROM carts WHERE sid = :sid";
    	$stmt = $this->conn->prepare($query);
    	$stmt->bindparam(":sid",$sid);
    	$stmt->execute();
    	$cart =  $stmt->fetchAll(PDO::FETCH_OBJ);
    	return $cart;
    }

	public function cartProductCount($user)
	{
		$query = "SELECT * FROM carts WHERE sid = :sid";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":sid",$user);
		$stmt->execute();
		$productCount =  $stmt->rowCount();
		return $productCount;
	}

}

 ?>