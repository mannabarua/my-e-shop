<?php

namespace Eshop\tag;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class tag
{
	public $conn;
	public $tag_id;
	public $title;
	public $picture;
	public $description;
    public $soft_delete;
	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  tags WHERE soft_delete = 0 ORDER BY tag_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $tags =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $tags;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO `tags`(`title`, `picture`, `description`) VALUES (:title, :picture, :description)";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":description",$this->description);
		$tag = $stmt->execute();
		return $tag;
    }
    public function build($data)
    {
    	$this->tag_id 		= $data['tag_id'];
    	$this->title 			= $data['title'];
    	$this->description 		= $data['description'];
    	$this->picture			= $data['picture'];
    	// $this->tag_type 	= $data['tag_type'];
    	// $this->is_new 			= $data['is_new'];
    	// $this->cost 			= $data['cost'];
    	// $this->mrp 				= $data['mrp'];
    	// $this->special_price 	= $data['special_price'];
    	// $this->is_active 		= $data['is_active'];
    	// $this->created_at 		= $data['created_at'];
    	// $this->modified_at 		= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  tags WHERE tag_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $tag =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$tag) 
        {
        	Messages::set('Sorry!.. tag is not found');
        	header('location: index.php');
        }
        return $tag;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM tags WHERE tag_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->picture);

		$query = "DELETE FROM tags WHERE tag_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('tag has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE tags SET soft_delete = 1 WHERE tag_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("tag remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> tag? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  tags WHERE soft_delete = 1 ORDER BY tag_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE tags SET soft_delete = 0 WHERE tag_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('tag restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `tags` SET `title`= :title,`picture`= :picture, ";
	    $query .= "`link`=:link,`description`=:description, ";
	    $query .= "`total_sales`=:total_sales WHERE tag_id = :id";
	    $result = $this->conn->prepare($query);
	    $result->bindparam(":title",$this->title);
	    $result->bindparam(":picture",$this->picture);
	    $result->bindparam(":description",$this->description);
	    $result->bindparam(":id",$this->tag_id);
	    $update =  $result->execute();
	    if ($update) {
	    	Messages::set('tag has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}