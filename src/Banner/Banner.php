<?php

namespace Eshop\Banner;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Banner
{
	public $conn;
	public $banner_id;
	public $title;
	public $link;
	public $promotional_messages;
	public $html_banner;
	public $picture;
	public $soft_delete;
	public $is_draft;
	public $is_active;
	public $max_display;
	public $created_at;
	public $modified_at;

	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM   banners WHERE soft_delete = 0 ORDER BY banner_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $products =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $products;
    }

    public function store($data)
    {
    	$this->build($data);

    	$query = "INSERT INTO banners(title, picture, promotional_messages	, is_draft, is_active, created_at, modified_at) VALUES (:title, :picture, :promotional_messages, :is_draft, :is_active, now(), now())";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":promotional_messages",$this->promotional_messages);
		$stmt->bindparam(":is_draft",$this->is_draft);
		$stmt->bindparam(":is_active",$this->is_active);
		$banner = $stmt->execute();
		return $banner;
    }
    public function build($data)
    {
    	$this->banner_id 		= $data['banner_id'];
    	$this->title 			= $data['title'];
    	$this->promotional_messages= $data['promotional_messages'];
    	$this->max_display 		= $data['max_display'];
    	$this->picture			= $data['picture'];
    	$this->is_draft 		= empty($data['is_draft']) ? 0 : $data['is_draft'];
    	$this->is_active 		= empty($data['is_active']) ? 0 : $data['is_active'];
    	$this->created_at 		= $data['created_at'];
    	$this->modified_at 		= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  banners WHERE banner_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $banner =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$banner) 
        {
        	Messages::set('Sorry!.. Product is not found');
        	header('location: index.php');
        }
        return $banner;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM products WHERE product_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->picture);

		$query = "DELETE FROM products WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('Product has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE products SET soft_delete = 1 WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("Product remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> product? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  products WHERE soft_delete = 1 ORDER BY product_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE products SET soft_delete = 0 WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('Product restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);
		// echo '<pre>';
		// var_dump($data);

	    $query = "UPDATE banners SET title= :title,picture= :picture, ";
	    $query .= "promotional_messages = :promotional_messages, ";
	    $query .= " is_draft = :is_draft, is_active = :is_active, modified_at = now()  WHERE banner_id = :id";
	    $stmt = $this->conn->prepare($query);
	    $stmt->bindparam(":title",$this->title);
	    $stmt->bindparam(":picture",$this->picture);
	    $stmt->bindparam(":promotional_messages",$this->promotional_messages);
	    $stmt->bindparam(":is_draft",$this->is_draft);
		$stmt->bindparam(":is_active",$this->is_active);
	    $stmt->bindparam(":id",$this->banner_id);
	    $update = $stmt->execute();
	    if ($update) {
	    	Messages::set('Banner has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;
	}


}