<?php

namespace Eshop\Product;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class Product
{
	public $conn;
	public $product_id;
	public $brand_id;
	public $label_id;
	public $title;
	public $picture;
	public $short_description;
	public $description;
	public $total_sales;
	public $product_type;
	public $is_new;
	public $cost;
	public $mrp;
	public $special_price;
	public $soft_delete;
	public $is_draft;
	public $is_active;
	public $created_at;
	public $modified_at;

	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  products WHERE soft_delete = 0 ORDER BY product_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $products =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $products;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO products(title, picture, short_description, description, total_sales, cost, mrp, is_draft, is_active, created_at, modified_at) VALUES (:title, :picture, :short_description, :description, :total_sales, :cost, :mrp, :is_draft, :is_active, now(), now())";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":short_description",$this->short_description);
		$stmt->bindparam(":description",$this->description);
		$stmt->bindparam(":total_sales",$this->total_sales);
		$stmt->bindparam(":cost",$this->cost);
		$stmt->bindparam(":mrp",$this->mrp);
		$stmt->bindparam(":is_draft",$this->is_draft);
		$stmt->bindparam(":is_active",$this->is_active);
		$product = $stmt->execute();
		return $product;
    }
    public function build($data)
    {
    	$this->product_id 		= $data['product_id'];
    	$this->title 			= $data['title'];
    	$this->short_description= $data['short_description'];
    	$this->description 		= $data['description'];
    	$this->total_sales 		= $data['total_sales'];
    	$this->picture			= $data['picture'];
    	// $this->product_type 	= $data['product_type'];
    	// $this->is_new 			= $data['is_new'];
    	$this->cost 			= $data['cost'];
    	$this->mrp 				= $data['mrp'];
    	// $this->special_price 	= $data['special_price'];
    	$this->is_draft 		= empty($data['is_draft']) ? 0 : $data['is_draft'];
    	$this->is_active 		= empty($data['is_active']) ? 0 : $data['is_active'];
    	$this->created_at 		= $data['created_at'];
    	$this->modified_at 		= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  products WHERE product_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $product =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$product) 
        {
        	Messages::set('Sorry!.. Product is not found');
        	header('location: index.php');
        }
        return $product;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM products WHERE product_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->picture);

		$query = "DELETE FROM products WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('Product has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE products SET soft_delete = 1 WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("Product remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> product? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  products WHERE soft_delete = 1 ORDER BY product_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE products SET soft_delete = 0 WHERE product_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('Product restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `products` SET `title`= :title,`picture`= :picture, ";
	    $query .= "`short_description` = :short_description,`description` = :description, ";
	    $query .= "`total_sales` = :total_sales, cost = :cost, mrp = :mrp, is_draft = :is_draft, is_active = :is_active, modified_at = now()  WHERE product_id = :id";
	    $stmt = $this->conn->prepare($query);
	    $stmt->bindparam(":title",$this->title);
	    $stmt->bindparam(":picture",$this->picture);
	    $stmt->bindparam(":short_description",$this->short_description);
	    $stmt->bindparam(":description",$this->description);
	    $stmt->bindparam(":total_sales",$this->total_sales);
	    $stmt->bindparam(":cost",$this->cost);
	    $stmt->bindparam(":mrp",$this->mrp);
	    $stmt->bindparam(":is_draft",$this->is_draft);
		$stmt->bindparam(":is_active",$this->is_active);
	    $stmt->bindparam(":id",$this->product_id);
	    $update = $stmt->execute();
	    if ($update) {
	    	Messages::set('Product has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}