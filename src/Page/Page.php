<?php

namespace Eshop\Page;

use Eshop\Db\Db;
use \PDO;
use Eshop\Utility\Messages;

class page
{
	public $conn;
	public $page_id;
	public $title;
	public $picture;
	public $link;
	public $description;
    public $soft_delete;
	function __construct(){
		$this->conn = Db::connect();
	}
    public function all()
    {
        $query = "SELECT * FROM  pages WHERE soft_delete = 0 ORDER BY page_id DESC";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $pages =  $stmt->fetchAll(PDO::FETCH_OBJ);
        return $pages;
    }

    public function store($data)
    {
    	$this->build($data);
    	$query = "INSERT INTO `pages`(`title`, `picture`, `link`, `description`) VALUES (:title, :picture, :link, :description)";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":title",$this->title);
		$stmt->bindparam(":picture",$this->picture);
		$stmt->bindparam(":link",$this->link);
		$stmt->bindparam(":description",$this->description);
		$page = $stmt->execute();
		return $page;
    }
    public function build($data)
    {
    	$this->page_id 		= $data['page_id'];
    	$this->title 			= $data['title'];
    	$this->link             = $data['link'];
    	$this->description 		= $data['description'];
    	$this->picture			= $data['picture'];
    	// $this->page_type 	= $data['page_type'];
    	// $this->is_new 			= $data['is_new'];
    	// $this->cost 			= $data['cost'];
    	// $this->mrp 				= $data['mrp'];
    	// $this->special_price 	= $data['special_price'];
    	// $this->is_active 		= $data['is_active'];
    	// $this->created_at 		= $data['created_at'];
    	// $this->modified_at 		= $data['modified_at'];
    }

    public function show($id)
    {
    	$query = "SELECT * FROM  pages WHERE page_id = :id";
        $stmt = $this->conn->prepare($query);
        $stmt->bindparam(":id",$id);
        $stmt->execute();
        $page =  $stmt->fetch(PDO::FETCH_OBJ);
        if (!$page) 
        {
        	Messages::set('Sorry!.. page is not found');
        	header('location: index.php');
        }
        return $page;
    }

    public function delete($id)
    {
    	$imgPath = DOCROOT.'/eshop/img/';
		$imgQuery = "SELECT * FROM pages WHERE page_id = :id";
		$result = $this->conn->prepare($imgQuery);
		$result->bindparam(":id",$id);
		$result->execute();

		$deleteImg = $result->fetch(PDO::FETCH_OBJ);
		unlink("$imgPath".$deleteImg->picture);

		$query = "DELETE FROM pages WHERE page_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$delete =  $stmt->execute();
		if ($delete) 
		{
			Messages::set('page has been deleted');
        	header('location: trash.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: trash.php');
		}
		return $delete;
    }

    public function softDelete($id)
    {
    	$query = "UPDATE pages SET soft_delete = 1 WHERE page_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);

		$sd = $stmt->execute();
		if ($sd) {
			Messages::set("page remove successfully. are you <a href='restore.php?id=".$id."'>restore</a> page? ");
        	header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
        	header('location: index.php');
		}
    }

	public function trash()
	{
		$query = "SELECT * FROM  pages WHERE soft_delete = 1 ORDER BY page_id DESC";
		$stmt = $this->conn->prepare($query);
		$stmt->execute();
		$trash =  $stmt->fetchAll(PDO::FETCH_OBJ);
		return $trash;
	}

	public function restore($id)
	{
		$query = "UPDATE pages SET soft_delete = 0 WHERE page_id = :id";
		$stmt = $this->conn->prepare($query);
		$stmt->bindparam(":id",$id);
		$sd = $stmt->execute();
		if ($sd) {
			Messages::set('page restore successfully..');
			header('location: index.php');
		}
		else
		{
			Messages::set('Sorry!.. There is a problem');
			header('location: trash.php');
		}
	}

	public function update($data)
	{
		$this->build($data);

	    $query = "UPDATE `pages` SET `title`= :title,`picture`= :picture, ";
	    $query .= "`link`=:link,`description`=:description, ";
	    $query .= "`total_sales`=:total_sales WHERE page_id = :id";
	    $result = $this->conn->prepare($query);
	    $result->bindparam(":title",$this->title);
	    $result->bindparam(":picture",$this->picture);
	    $result->bindparam(":link",$this->link);
	    $result->bindparam(":description",$this->description);
	    $result->bindparam(":id",$this->page_id);
	    $update =  $result->execute();
	    if ($update) {
	    	Messages::set('page has been update successfully');
            header("location: index.php");
	    }else{
	    	Messages::set('Sorry!.. There is a problem. Please try again');
            header("location: index.php");
	    }
  		return $update;

	}

}