<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
      <link rel="manifest" href="site.webmanifest">
      <link rel="apple-touch-icon" href="icon.png">
      <!-- Place favicon.ico in the root directory -->
      <!-- Font Awesome -->
      <link rel="stylesheet" href="../css/all.min.css">

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../css/bootstrap.min.css">

      <!-- Main Stylesheet -->
      <link rel="stylesheet" href="../css/main.css"> 


    <title>Admin Panel</title>
  </head>
  <body>
   
   <div class="body-wrapper"> <!-- Full body wrap -->

     	<aside id="sidebar"> <!-- Sidebar Start  -->
        <div class="sidebar-body">
       		<div class="sidebar-header">
       			<div class="logo-section">
              <img src="../img/logo-white.png" alt="" style="width:140px;">
            </div>
            <!-- admin Info -->
            <div class="admin-info ">
                <div class="admin-image">
                    <img src="../img/jishat.jpg" width="48px" height="48px" alt="User" />
                </div>
                <div class="info-container">
                    <h2>AR Jishat</h2>
                    <h3>arjishat@gmail.com</h3>
                </div>
            </div>
       		</div>   		
       		<nav class="sidebar-menu">
            <ul class="list-unstyled ">
              <h2>MAIN NAVIGATION</h2>
              <li>
                <a href="#"><i class="fas fa-tachometer-alt"></i> Dashboard</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="collapse" class="dropDownMenu" data-target="#subMenuOne" aria-expanded="false"> <i class="fas fa-tachometer-alt"></i> Home Page</a>
                <ul  id="subMenuOne" class="collapse list-unstyled">
                  <li>
                    <a href="#">Logo</a>
                  </li>
                  <li>
                    <a href="#">Banner</a>
                  </li>
                </ul> 
              </li>              
              <li>
                <a href="view.php"><i class="fas fa-tachometer-alt"></i> Banner</a>
              </li>
              <li>
                <a href="#"><i class="fas fa-tachometer-alt"></i> About</a>
              </li>
              <li>
                <a href="javascript:void(0)" data-toggle="collapse" data-target="#subMenuTwo"> <i class="fas fa-tachometer-alt"></i> Home</a>
                <ul class="collapse list-unstyled" id="subMenuTwo">
                  <li>
                    <a href="#">home1</a>
                  </li>
                  <li>
                    <a href="#">home2</a>
                  </li>
                  <li>
                    <a href="#">home3</a>
                  </li>
                </ul> 
              </li> 
          </ul> 
          </nav>       		
        </div>

        <div class="sidebar-button">
          <button type="button" id="sidebarCollapse" class="btn btn-info">
            <i class="fa fa-align-justify"></i> <span></span>
          </button>  
        </div>        
     	</aside>
      <!-- Sidebar Start  End-->

     	<div id="body-content"> <!-- body content start -->

        <div class="tob-bar">
          <div class="container-fluid">
            <div class="row">
              <div class="col-12">
                <nav class="navbar navbar-light ">   
                  <ul class="navbar-nav tob-bar-menu">
                      <li><a href="#"> <i class="far fa-bell"></i></a></li>
                      <li><a href="#">Sign Out</a> </li>
                  </ul>
                </nav>
              </div>
            </div>
       	  </div>
        </div>

        <div class="table-section shadow">
          <div class="container-fluid">
            <div class="row mb-3 overflow-hidden">
              <div class="col-12">
                <h2> Add New</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-12">
                <div class="add-form">
                  <form id="contact-form" method="post" action="store.php" role="form">

                    <div class="messages"></div>                    
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="id">ID</label>
                                <input id="id"  value="" type="text" name="id" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="brand_id">brand_id</label>
                                <input id="brand_id"  value="" type="text" name="brand_id" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="label_id">label_id</label>
                                <input id="label_id"  value="" type="text" name="label_id" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="title">title</label>
                                <input id="title"  value="" type="text" name="title" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="picture">picture</label>
                                <input id="picture"  value="" type="text" name="picture" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="short_description">short_description</label>
                                <input id="short_description"  value="" type="text" name="short_description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="description">description</label>
                                <input id="description"  value="" type="text" name="description" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="total_sales">total_sales</label>
                                <input id="total_sales"  value="" type="text" name="total_sales" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="product_type">product_type</label>
                                <input id="product_type"  value="" type="text" name="product_type" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_new">is_new</label>
                                <input id="is_new"  value="" type="text" name="is_new" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="cost">cost</label>
                                <input id="cost"  value="" type="text" name="cost" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="mrp">mrp</label>
                                <input id="mrp"  value="" type="text" name="mrp" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="special_price">special_price</label>
                                <input id="special_price"  value="" type="text" name="special_price" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="soft_delete">soft_delete</label>
                                <input id="soft_delete"  value="" type="text" name="soft_delete" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_draft">is_draft</label>
                                <input id="is_draft"  value="" type="text" name="is_draft" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="is_active">is_active</label>
                                <input id="is_active"  value="" type="text" name="is_active" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="created_at">created_at</label>
                                <input id="created_at"  value="" type="text" name="created_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="modified_at">modified_at</label>
                                <input id="modified_at"  value="" type="text" name="modified_at" class="form-control">
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info"><a href="tableShow.html" style="text-decoration: none; color: white">Save</a></button>
                    <!--<input type="submit" class="btn btn-success btn-send" value="Send & Save message">-->
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

       

   	  </div><!-- body content End -->  
   	
   	
  </div> <!-- Full body wrap ENd-->
    

    <!--  JavaScript link -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
  
    
    
    
    
    
  </body>
</html>