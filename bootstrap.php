<?php

	include_once('vendor/autoload.php');
	session_start();
    

    if (array_key_exists('guest_user', $_SESSION) && !empty($_SESSION['guest_user'])) {
        $_SESSION['guest_user'];
    }else{
        $_SESSION['guest_user'] = 'guest_user_'.time();
    }

    define('DOCROOT', $_SERVER['DOCUMENT_ROOT']);
    define('WEBROOT', 'http://localhost/eshop/');
    define('CSS', 'http://localhost/eshop/css/');
    define('JS', 'http://localhost/eshop/js/');
    define('UPLOAD', 'http://localhost/eshop/');
    define('IMG', 'http://localhost/eshop/img/');
    define('ELEMENT', 'http://localhost/eshop/admin/view/element/');
    define('BANNER', 'http://localhost/eshop/admin/view/banners/');
    define('PAGE', 'http://localhost/eshop/admin/view/page/');
    define('BRANDS', 'http://localhost/eshop/admin/view/brands/');
    define('CATEGORY', 'http://localhost/eshop/admin/view/category/');
    define('PRODUCTS', 'http://localhost/eshop/admin/view/products/');
    define('DESHBOARD', 'http://localhost/eshop/admin/view/deshboard/');
    define('TESTIMONIAL', 'http://localhost/eshop/admin/view/testmonial/');
    define('FRONTENEMET', DOCROOT.'/eshop/front/element/');
    define('PRODUCTDETAIL', 'http://localhost/eshop/front/');


?>

